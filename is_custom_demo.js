// ==UserScript==
// @name         IS Custom Demos v8.0.3
// @namespace    http://tampermonkey.net/
// @version      8.0.3
// @description  try to take over the world with interaction studio!
// @author       Juliano Polito
// @match        http*://*/*
// @require		 https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @grant        GM_xmlhttpRequest
// @connect      evergage.com
// ==/UserScript==

/***************************/
/****** INSTRUCTIONS ******

For instructions visit:
https://bitbucket.org/julianopolito/is_custom_demo/src/master/README.md

****************************/
// Should run only on topFrame?
var topFrame = true;

if(topFrame){
   if (window.top != window.self){
       //log("not top frame");
       return;
   }
}


var purl = window.location.href;
log('init', purl);

/*****************************
****** USER PARAMETERS *******
******************************/
//SET the IS account and dataset you want to send events to
//Account is the subdomain of evergage
var accountIS = "interactionstudio";
var instanceIS = "us-4";
var datasetIS = "nto2";

var accountISname = accountIS.split(".")[0];

var mapping = [
    //MAPPING EXAMPLE 1
    {
        mapURL: "https://www.samsung.com/br/",
        personalize:[
            {selector:"#content > div > div > div.responsivegrid.aem-GridColumn.aem-GridColumn--default--12 > div > div.ho-g-home-kv-carousel.aem-GridColumn.aem-GridColumn--default--12",
             img:"https://www.nutechnologyinc.com/nu_blog/wp-content/uploads/2018/06/CRM-BANNER-1.png",
             link:"https://www.salesforce.com"
            }
        ],
        userID:"jpolito@salesforce.com",
        productID:"1050943", // OPTIONAL
        action:"Browse Samsung Home"
    }//END OF MAPPING 1
];





/************************
IF THE SCRIPT DOESN'T WORK, TRY CHANGING TO A HIGHER DELAY
Sometimes the page takes too much time to load, and the DOM/selectors are not available yet
************************/
var scriptDelay = 2000;
log("script will run in ", scriptDelay/1000, "seconds");

var resolvedUserID = null;
var localstoKey = "__iscustomdemo";
var localsto = getSto();
log("user map", localsto);

setTimeout(
function() {
    'use strict';

    //Press unmapped key to clear flag
    $(document).keypress(function(e){
        if (e.target.nodeName.toLowerCase() !== 'input') {
            log("key pressed ", e.keyCode);
            var flag = 0;
            for (const i of mapping){
                if(i.hasOwnProperty("key") && i.key.toLowerCase() == e.key.toLowerCase()){
                    if(i.hasOwnProperty("mapURL") && i.mapURL != "" && i.mapURL != purl){
                       log("mapped key pressed, URL doesn't match", i.key);
                        continue;
                    }
                    localStorage.setItem("sfdemoObj", JSON.stringify(i));
                    if(i.action){
                        sendISEvent(i);
                    }
                    log("i key set: ", i);
                    flag = 1;
                }
            }
            if(!flag){
                log("clearing key map stored object - key not mapped ", e.key.toLowerCase());
                localStorage.setItem("sfdemoObj", null);
            }
        }
    });

    $(function(){

        var config = findConfig();

        if(!config){
            log("no url mapping found for current page. Trying to find key mapped object");
            var demoObjStr = localStorage.getItem("sfdemoObj");
            if(!demoObjStr){
               log("no key mapped object found - exit");
               return;
            }else{
                var demoObj = JSON.parse(demoObjStr);
                config = demoObj;
                log("key mapped object found: ", demoObj);
            }
        }

        //Style for responsive image added to head
        var sty = $('<style>');
        sty.html('.responsive {width: 100%;height: auto;}');
        $('head').append(sty);


        log("trying to find elements by selector for personalization");
        if(!config.hasOwnProperty("personalize") || !config.personalize instanceof Array){
            log("personalize array not found for mapping.");
            return;
        }
        for (const p of config.personalize){
            var containerEL = $(p.selector).first();

            if(containerEL.length > 0){
                log("container found", p.selector);
            }else{
                log("container NOT found - skipping ", p.selector);
                continue;
            }

            log("personalization obj: ", p);

            //Target
            var linkTarget = "_self";
            if(p.hasOwnProperty("linkTarget")){
                linkTarget = p.linkTarget;
                log("Found link target", linkTarget);
            }
            //containerEL.html("");
            if(p.hasOwnProperty("img") && p.img){
                //External div container for image
                var div = $('<div>');
                div.addClass("_-_demojp_-_");
                div.css('width','100%');
                //div.css('height', adH)
                div.css('text-align', 'center');
                div.css('margin-bottom', '20px');
                if(p.hasOwnProperty("style")){
                    div.css(p.style);
                    log("Found div style", p.style);
                }

                //Img element for the banner
                //var imgURL = 'http://image.s4.exct.net/lib/fe851574726c0d7b72/m/1/8144b51d-61ef-4d68-b9fe-47866a1b96cb.jpg';
                var imgEL = $('<img>');
                imgEL.attr('src', p.img);
                //imgEL.css('width',adW);
                //imgEL.css('height', adH);
                imgEL.addClass('responsive');
                if(p.hasOwnProperty("imgStyle")){
                    imgEL.css(p.imgStyle);
                    log("Found img style", p.imgStyle);
                }
                //Lin element for redirect
                var linkEL = $('<a>').css('text-decoration', 'none').css('cursor', 'pointer').attr('href', p.link).attr('target', linkTarget);

                if(p.hasOwnProperty("link") && p.link != ''){
                    log("link found. binding link to image");
                    linkEL.append(imgEL);
                    div.append(linkEL);
                }else{
                    log("link is empty. skipping link bind.");
                    div.append(imgEL);
                }
                //sets default addType as replace
                if(!p.hasOwnProperty("advanced") || !p.advanced.hasOwnProperty("addType")){
                    p["advanced"] = {"addType":"replace"};
                }

                if(p.hasOwnProperty("advanced") && p.advanced.hasOwnProperty("addType")){
                    if(p.advanced.addType == "replace"){
                        //containerEL.empty();
                        containerEL.after(div);
                        containerEL.remove();
                        log("container addType replace", p.selector);
                    }else if(p.advanced.addType == "append"){
                        containerEL.append(div);
                        log("container addType append", p.selector);
                    }else if(p.advanced.addType == "prepend"){
                        containerEL.prepend(div);
                        log("container addType prepend", p.selector);
                    }else if(p.advanced.addType == "after"){
                        containerEL.after(div);
                        log("container addType after", p.selector);
                    }else if(p.advanced.addType == "before"){
                        containerEL.before(div);
                        log("container addType before", p.selector);
                    }
                }
            }else if(p.hasOwnProperty("text")){
                containerEL.text(p.text);
                containerEL.on( "click", function() {
                    if(linkTarget=="_self"){
                        window.location.href = p.link;
                    }else if(linkTarget=="_top"){
                        window.top.location.href = p.link;
                    }else if(linkTarget=="_blank"){
                        var newWin=window.open(p.link,'newww__');
                    }
                    return false;
                });
            }else if(p.hasOwnProperty("html")){
                containerEL.html(p.html);
                containerEL.on( "click", function() {
                    if(linkTarget=="_self"){
                        window.location.href = p.link;
                    }else if(linkTarget=="_top"){
                        window.top.location.href = p.link;
                    }else if(linkTarget=="_blank"){
                        var newWin=window.open(p.link,'newww__');
                    }
                    return false;
                });
            }
        }
	});

}, scriptDelay);

var matches = {};
var currentMapping = {};

function findConfig(){
    for (const i of mapping){
        if(i.hasOwnProperty("mapURL") && typeof i.mapURL !== 'undefined'){
            if(i.mapURL.constructor == String && i.mapURL == purl){
                log("found URL mapping exact match", purl);
                currentMapping = i;
                sendISEvent(i);
                return i;
            }else if(i.mapURL.constructor == RegExp && i.mapURL.test(purl)){
                log("found URL mapping pattern", purl);
                currentMapping = i;
                matches = i.mapURL.exec(purl);
                log("Matches", matches);
                sendISEvent(i);
                return i;
            }
        }
    }
    return null;

}

setInterval(function(){
    try{
        sendISTimeEvent(currentMapping, 59999);
    }catch(e){
        log(e);
    }
},10000);

function sendISEvent(i){
    if(!i.hasOwnProperty("action")){
        log("action not found for config. action not sent to IS");
        return;
    }
    var eventParams = { action: i.action, user: {userID:"", attributes:{}}, flags: {noCampaigns: true}, source:{channel:"Web"}};
    if(i.hasOwnProperty('userID')) {
        eventParams.user.id = i.userID;
        eventParams.user.name = i.userID;
        eventParams.user.attributes.userName = i.userID;
        if(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(i.userID)){
            eventParams.user.attributes.emailAddress = i.userID;
            log("id is email", i.userID);
        }
    };
    if(i.hasOwnProperty('sfmcContactKey')) {
        eventParams.user.attributes.sfmcContactKey = i.sfmcContactKey;
    }
    if(typeof(eventParams.user.anonId) === 'undefined' && typeof(eventParams.user.id) === 'undefined'){
        log("action id not set, generating anonid");
        eventParams.user.anonId = uuidv4();
    }
    if(i.hasOwnProperty('kruxID')) eventParams.user.attributes.kruxID = i.kruxID;
    if(i.hasOwnProperty('attributes')){
        //
        var att = {...eventParams.user.attributes, ...i.attributes}
        eventParams.user.attributes = att;
    }
    if(i.hasOwnProperty('productID')){
        //https://interactionstudio.evergage.com/twreceiver?_ak=interactionstudio&_ds=bancoitau&.item=%7B%22_id%22%3A%22Marketing+Cloud%22%2C%22type%22%3A%22p%22%7D&action=View+Item&userId=jdrp&_=1606938101731
        var urlI = "";
        var prodID = replaceRegex(i.productID);
        log("View Item", prodID);
        var viewObject = {
            action:"View Item",
            itemAction: "View Item",
            catalog: {
                Product: {
                    "_id": prodID,
                    "id": prodID
                }
            }
        };

        var eventParamsV = {...eventParams, ...viewObject};
        var strfyV = JSON.stringify(eventParamsV);
        strfyV = replaceRegex(strfyV);
        log("sending action to IS - eventParamsV: ", JSON.parse(strfyV));
        var b64V = btoa(strfyV);
        var ret3V = GM_xmlhttpRequest({
            method: "GET",
            url: "https://"+accountIS+"."+instanceIS+".evergage.com/api2/event/"+datasetIS+"?event="+b64V,
            onload: function(res) {
                try{
                    log( "success view item api event", res.responseText );
                }catch(e){}
            },
            onerror: function(res) {
                try{
                    log( "error view item api event", res.error );
                }catch(e){}
            }
        });

       if(i.hasOwnProperty("purchase")){

            if(i.purchase.hasOwnProperty("price")){
                var price = parseFloat(i.purchase.price);
                var qty = 1;
                if(i.purchase.hasOwnProperty("qty"))qty = parseInt(i.purchase.qty);

                var purchaseObject = {
                    action:"Purchase",
                    itemAction: "Purchase",
                    "order":
                    {"Product":
                     {"orderId":"GBEZ"+Math.floor(Math.random()*100000000),
                      "totalValue":price*qty,
                      "currency":"USD",
                      "lineItems": [
                          {_id: prodID,
                           price: price,
                           quantity:qty}]
                     }
                    }
                };

                log("Purchase", purchaseObject);

                var eventParams2 = {...eventParams, ...purchaseObject};
                var strfyP = JSON.stringify(eventParams2);
                strfyP = replaceRegex(strfyP);
                log("sending action to IS - eventParams2: ", JSON.parse(strfyP));
                var b64P = btoa(strfyP);
                var ret3P = GM_xmlhttpRequest({
                    method: "GET",
                    url: "https://"+accountIS+"."+instanceIS+".evergage.com/api2/event/"+datasetIS+"?event="+b64P,
                    onload: function(res) {
                        try{
                            log( "success purchase api event", res.responseText );
                        }catch(e){}
                    },
                    onerror: function(res) {
                        try{
                            log( "error purchase api event", res.error );
                        }catch(e){}
                    }
                });
            }
       }
    }
    if(i.hasOwnProperty('categoryID')){
        var catID = replaceRegex(i.categoryID);
        log("View Category", catID);
        var viewCategory = {
            action:"View Category",
            itemAction: "View Category",
            catalog: {
                Category: {
                    "_id": catID,
                    "id": catID
                }
            }
        };

        var eventParamsC = {...eventParams, ...viewCategory};
        var strfyC = JSON.stringify(eventParamsC);
        strfyC = replaceRegex(strfyC);
        log("sending action to IS - eventParamsC: ", JSON.parse(strfyC));
        var b64C = btoa(strfyC);
        var ret3C = GM_xmlhttpRequest({
            method: "GET",
            url: "https://"+accountIS+"."+instanceIS+".evergage.com/api2/event/"+datasetIS+"?event="+b64C,
            onload: function(res) {
                try{
                    log( "success view cat api event", res.responseText );
                }catch(e){}
            },
            onerror: function(res) {
                try{
                    log( "error view cat api event", res.error );
                }catch(e){}
            }
        });
    }

    var strfy = JSON.stringify(eventParams);
    strfy = replaceRegex(strfy);
    log("sending action to IS - eventParams: ", JSON.parse(strfy));
    var b64 = btoa(strfy);
    var ret3 = GM_xmlhttpRequest({
            method: "GET",
            url: "https://"+accountIS+"."+instanceIS+".evergage.com/api2/event/"+datasetIS+"?event="+b64,
            onload: function(res) {
                try{
                    log( "success api event", res.responseText );
                    var campResponse = JSON.parse(res.responseText);
                    log("Parsed Response", campResponse);
                    resolvedUserID = campResponse.resolvedUserId ? campResponse.resolvedUserId : campResponse.id;
                    setUserSto(i.userID, resolvedUserID);
                    // {"id":"6112f02a87ace83591bcb918","campaignResponses":[],"errorCode":0,"persistedUserId":{"entityId":"oxx4QeHuIszQayJCDfTLxdxzVOlAkNbNerAMtVOh7bj_-C8wFSRHS8CiZYtksnVj7A3zDdKur_76AD6AFeXH1MP_Ixad_waXZxKyCC9WUeQEfwloTbLPWI0fcG2rCUbw","accountId":""},"resolvedUserId":"a5610c2bc4cde12c07c179af"}
                }catch(e){
                    log(e);
                }
            },
            onerror: function(res) {
                try{
                    log( "error api event", res.error );
                }catch(e){}
            }
        });
}

function sendISTimeEvent(i, time){
    if(i && i.hasOwnProperty("productID")){
        var urlT = "";
        if(i.userID){
            if(!resolvedUserID){
                log("no resolveduser found, abort time event");
                return;
            }
            urlT = "https://"+accountIS+".evergage.com/pr?_ak="+accountISname+"&_ds="+datasetIS+"&.item=%7B%22_id%22%3A%22"+i.productID+"%22%2C%22type%22%3A%22p%22%7D&action=View+Time&.top="+time+"&userId="+encodeURIComponent(resolvedUserID)+"&userName="+encodeURIComponent(resolvedUserID)+"&_="+Date.now()
        }else if(i.anonID){
            urlT = "https://"+accountIS+".evergage.com/pr?_ak="+accountISname+"&_ds="+datasetIS+"&.item=%7B%22_id%22%3A%22"+i.productID+"%22%2C%22type%22%3A%22p%22%7D&action=View+Time&.top="+time+"&.anonId="+encodeURIComponent(i.anonID)+"&_anon=true&_="+Date.now()
        }else{
            urlT = "https://"+accountIS+".evergage.com/pr?_ak="+accountISname+"&_ds="+datasetIS+"&.item=%7B%22_id%22%3A%22"+i.productID+"%22%2C%22type%22%3A%22p%22%7D&action=View+Time&.top="+time+"&.anonId="+uuidv4()+"&_anon=true&_="+Date.now()
        }
        log("sending "+Math.floor(time/1000)+" seconds interaction signal to IS for productID:", i.productID);
        log("time url", urlT);
        var ret = GM_xmlhttpRequest({
            method: "GET",
            url: urlT,
            onload: function(res) {
                try{
                    log( "success time event", res.responseText );
                }catch(e){}
            },
            onerror: function(res) {
                try{
                    log( "error time event", res.error );
                }catch(e){}
            }
        });
    }
}
function replaceRegex(str){
    var rp = /\{\:(\d+)\}/g;
    var result;
    var results = [];
    while((result = rp.exec(str)) !== null) {
        results.push({pattern: result[0], index:result[1]});
    }
    for (const i of results) {
        if(matches[i.index]){
            log("replacing ", i.pattern, matches[i.index]);
            str = str.replaceAll(i.pattern, matches[i.index]);
        }else{
            log("replacing ", i.pattern, "");
            str = str.replaceAll(i.pattern, "");
        }
    }
    //log("str", str);
    return str;
}

function getCookie(name) {
  const value = decodeURIComponent(document.cookie);
  //log("Cookie", value);
  const parts = value.split(name);
  if (parts.length === 2){
      var v = parts.pop().split(';').shift().substring(1);//replace("=","");
      log("Cookie", name, " found: ", v);
      return v;
  }else{
      log("Cookie ", name, " not found");
  }
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

function saveSto(){
    var str = JSON.stringify(localsto);
    log("saving sto ", str);
    window.localStorage.setItem(localstoKey, str);
}

function initSto(){
    var ret = {user2resolved:{}, resolved2user:{}};
    log(window.localStorage);
    try{
        var st = window.localStorage.getItem(localstoKey);
        if(!st){
         throw new Exception("sto not found");
        }
        log("st", st);
        ret = JSON.parse(st);
        log("sto found", ret);
    }catch(e){
        log("sto NOT found. setting to ", ret);
        localsto = ret;
        saveSto();
    }
}

function setUserSto(userID, resolvedID){
    localsto.user2resolved[userID] = resolvedID;
    localsto.resolved2user[resolvedID] = userID;
    saveSto();
}

function getResolvedID(userID){
    try{
        return localsto.user2resolved[userID];
    }catch(e){
        return null;
    }
}

function getUserIDfromResolvedID(resolvedID){
    try{
        return localsto.resolved2user[resolvedID];
    }catch(e){
        return null;
    }
}

function getSto(){
    initSto();
    try{
        var st = window.localStorage.getItem(localstoKey);
        localsto = JSON.parse(st);
        log("sto found", localsto);
    }catch(e){
    }
    return localsto;
}

function clearSto(){
    window.localStorage.removeItem(localstoKey);
}

function log(...args){
    var domain = "";
    if(purl){
        try{
            domain = /:\/\/([^\/]+)/.exec(purl)[1];
        }catch (e){
            console.log("DEMOLOG error logging domain");
        }
    }
    console.info("[DEMOLOG]","["+ domain + "]", ...args);
}


