# IS Custom Demo Tampermonkey

For simulating Interaction Studio 2.0 sitemap actions, tracking and personalization in a couple minutes.  
It is able to replace content on a webpage to simulate the WebSDK personalization,  
and it also sends API signals to interaction studio for you to demo IS working in realtime, without all the hassle of real implementation.

## Author

**Juliano Polito**
jpolito@salesforce.com

## Getting Started

### Prerequisites

You should install Tampermonkey chrome extension 
```
https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en
```
and copy this script
```
https://bitbucket.org/julianopolito/is_custom_demo/src/master/is_custom_demo.js
```
to a blank script in Tampermonkey.

You should also have already setup the backend as stated in IS Custom Demo training, such as:  

* Setup the catalog 
    * Enable the default Dimensions: Product and Category
    * Create the Affinity Wheel dimensions: ItemClass, Brand, Style
* Add dimension values using the UCP Tool
* Add products and associate to the dimensions


## How to use 


### The Basics

1. Set the IS account and dataset variables - this is the dataset that will receive the events in event stream
```javascript
var accountIS = "interactionstudio";
var datasetIS = "nto2";
```
1. Set a mapping object for each URL/Webpage where you want to trigger an action and personalization using mapURL attribute. For each mapping you can set an action, an userID, a productID and personalizations.
	1. For each personalize object set:
    	* selector: the selector for the container in the page where content will be injected
    	* img: An image URL to be injected into the selector container / page
    	* link: The clickthrough of the new image
	1. userID: OPTIONAL set the userID of the IS profile you are targeting
	1. productID: OPTIONAL add a product ID of an existing catalog product to register Item View item action.  
    __*When a productID is set, it will populate the affinity wheel, update view count and viewtime*__ 
    1. categoryID: OPTIONAL add a category ID of an existing catalog category to register Category View item action.  
    __*When a productID is set, it will populate the affinity wheel, update view count and viewtime*__ 
    1. purchase: OPTIONAL add a price to the product to to register Purchase item action for the defined productID.  
    __*When set {price: 300}*__ 
	1. action: The action you want to see triggered in Event Stream

#### Mapping Basic Example  

This is what a basic setup looks like.  
First we set the **mapURL**, so that whenever we navigate to that URL, we trigger the script behavior described below.  
In this example, we find the element set in the **selector** and replace it with the **img**, and set the link of the new image to **link**.  
Then the **action** is sent to the _Event Stream_ and associated to the profile of the user **userID**.  
The product identified by **productID** will have its view count and view time updated.    
  
```javascript
//
var mapping = [
    //MAPPING EXAMPLE 1.1
    {
        mapURL: "https://www.samsung.com/br/",
        personalize:[
            {selector:"#content > div > div > div.responsivegrid.aem-GridColumn.aem-GridColumn--default--12 > div > div.ho-g-home-kv-carousel.aem-GridColumn.aem-GridColumn--default--12",
             img:"https://www.nutechnologyinc.com/nu_blog/wp-content/uploads/2018/06/CRM-BANNER-1.png",
             link:"https://www.salesforce.com"
            }
        ],
        userID:"jpolito@salesforce.com",
        productID:"1050943", // OPTIONAL
        action:"Browse Samsung Home"
    }//END OF MAPPING 1.1
];
```
    
You can also completely remove the **personalize** and **productID** if you just want to capture the action.  
This will only send the **action** to the _Event Stream_ and associate id to the profile of **userID**.  
  
```javascript
//
var mapping = [
    //MAPPING EXAMPLE 1.2
    {
        mapURL: "https://www.samsung.com/br/",
        userID:"jpolito@salesforce.com",
        action:"Browse Samsung Home"
    }//END OF MAPPING 1.2
];
```

For anonymous capture, change the **userID** for **anonID**.  
In this case, instead of associating the **action** to the profile of a known user, it will associate to an unknown user profile.  

```javascript
//
var mapping = [
    //MAPPING EXAMPLE 1.3
    {
        mapURL: "https://www.samsung.com/br/",
        action:"Browse Samsung Home"
    }//END OF MAPPING 1.3
];
```

Simplest example for text personalization.
 
```javascript
//
var mapping = [
    //MAPPING EXAMPLE 1.4
    {
        mapURL: "https://www.salesforce.com/",
        personalize:[
            {selector:"#develop-deeper-customer-relationships-with-loyalty-management > span",
             text:"Need help?",
             link:"https://www.salesforce.com"
            }
        ]
    }//END OF MAPPING 1.4
];
```

### Advanced Use

More complete example of a mapping object.  

```javascript
//
 {//MAPPING EXAMPLE 2.1
        mapURL: /(https?):\/\/www.samsung.com\/(\w*)?\/tvs\/all-tvs\/\??(.*)?/i,
        personalize:[
            {selector:"#content > div > div > div.responsivegrid.aem-GridColumn.aem-GridColumn--default--12 > div > div.ho-g-home-kv-carousel.aem-GridColumn.aem-GridColumn--default--12",
             img:"https://www.nutechnologyinc.com/nu_blog/wp-content/uploads/2018/06/CRM-BANNER-1.png",
             link:"https://www.salesforce.com",
             linkTarget:"_self",
             style: {border:"2px solid red"},
             advanced:{addType:"replace"}
            },
            {selector:"#content > div > div > div.responsivegrid.aem-GridColumn.aem-GridColumn--default--12 > div > div:nth-child(3)",
             img:"https://www.exafort.com/images/stories/salesforce-banner.jpg",
             link:"https://www.salesforce.com",
             linkTarget:"_self",
             style: {border:"2px solid yellow"},
             advanced:{addType:"replace"}
            },
            ,
            {selector:"#content > div > div > div.responsivegrid.aem-GridColumn.aem-GridColumn--default--12 > div > div:nth-child(3)",
             text:"New Text"
            }
            
        ],
        userID:"jpolito@salesforce.com",
        sfmcContactKey:"jpolito@salesforce.com",
        attributes:{
            FirstName:"Juliano Polito",
            city:"Sao Paulo",
            mobilePhone:"11993355213",
            interest:"{:2}{:3}"
        },
        productID:"1050943",
        purchase:{price:2098},
        action:"Buy Product {:3}"
    }//END OF MAPPING 2.1
```

Similar example, but using Key Binding instead of URL mapping  

```javascript
 {//MAPPING EXAMPLE 2.2
        key: "f",
        personalize:[
            {selector:"#content > div > div > div.responsivegrid.aem-GridColumn.aem-GridColumn--default--12 > div > div.ho-g-home-kv-carousel.aem-GridColumn.aem-GridColumn--default--12",
             img:"https://www.nutechnologyinc.com/nu_blog/wp-content/uploads/2018/06/CRM-BANNER-1.png",
             link:"https://www.salesforce.com",
             linkTarget:"_self",
             style: {border:"2px solid red"},
             advanced:{addType:"replace"}
            }
        ],
        userID:"jpolito@salesforce.com",
        attributes:{
            FirstName:"Juliano Polito",
            city:"Sao Paulo",
            mobilePhone:"11993355213",
            interest:"TV"
        },
        productID:"1050943",
        action:"Browse Product TV"
    }//END OF MAPPING 2.2
```

#### Advanced features of mapping:

1. Use of regex patterns and groups for URL mapping
    * [Regexp](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp) 
    * [Regexp Examples](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp#Examples)
1. Personalize: you can set multiple elements to be personalized in the same page    
1. Personalize object attributes:
    * selector: the selector for the container in the page where content will be injected. It can be any container element. It could be a div, span, anchor, even a button.
    * img: An image URL to be injected into the page to fake personalization.
    * text: Instead of an image you can set a new text for the element (added in 7.5)
    * html: Instead of an image or simple text, you can replace the contents of the element with HTML code (added in 7.5)
    * link: The clickthrough of the new image. When the user clicks the new **img** it will redirect to the link set here.
	* linkTarget - you can set _top or _blank for the link - [link target attribute](https://www.w3schools.com/tags/att_a_target.asp)
	* style - you can set the style of the div being injected to further control the personalization - [style attribute](https://www.w3schools.com/tags/att_style.asp)
	* you can set **advanced.addType** to modify how the DIV is injected into the page:
        * replace - the default behavior. Removes the original element, and injects the new one
        * before - Doesn't remove the original object, and places the new object before it.
        * after - Doesn't remove the original object, and places the new object after it.
        * append - Doesn't remove the original object, and places the new object inside it as the last object.
        * prepend - Doesn't remove the original object, and places the new object inside it as the first object.
1. User IDs - you can set anonID instead of userID if it is anonymous, and you can set both to force identity merging
    * To demonstrate identity merging, you should have 2 mappings. In the first one you set only the **anonID**, and then in the second one you set both **anonID** and **userID**,  
    where **anonID** will keep the same value of the first mapping. This will make IS show the anon actions in the event stream of the known user profile.
    * In example 2.1 we are setting **anonID** from a cookie value. You can also set it to a string.
1. Profile attributes - by using the attributes object, you can update the profile attributes in realtime and show it in the unified customer profile in IS
    * The attribute must exist in IS prior to using it here
    * You can create realtime segments by using the attributes
1. Regex replacing - you can set groups in the URL patterns. Then you can use the group content in any mapping string - {:1} group 1, {:2} group 2 ... 
1. In example 2.2 we are using a keyboard stroke as the trigger. The personalization will then happen in the next refresh of whatever page you navigate to. The action though, is sent to IS at the time you press the key.  
    * This can be used without personalization to fake external actions, like in-store, ATM, POS actions.
    * Note that in this case we cannot use string replacements as we do not have the URL pattern groups

## Tips and Notes

* You can use the getCookie(cookieName) function to get cookie values from the website
* URL exact mapping will also consider URL parameters, so the three URLs below are considered different
    * "https://www.samsung.com/br/"
    * "https://www.samsung.com/br/?is"
    * "https://www.samsung.com/br/?utm_campaign=tv"
* At least one trigger must be set per item mapping, you can set both if needed, but no less than one
* If you press an unmapped key, it will clear the selection
* If you open DEV TOOLS on the browser, filter the console for DEMOLOG entries and you can see all actions that are being performed for debugging purposes
* If script not working because website is too slow to load (if it says selector not found for example), you can try changing the scriptDelay variable to a higher number to compensate for that.
    * Many websites take to long to load their pages (the DOM) and it doesnt work because when the script runs the objects/selectors don't exist.
    * So increasing the delay of the script execution can help. Default is 3000 milliseconds (3 seconds)
* You can set topFrame to false if you need to replace elements inside iframes - but be aware that the script will run multiple times and with different delays, as soon as each page gets loaded
* If you set productID, every 10 seconds spent on the page will trigger 1 minute of interaction with said product, faking real world view time

## Changelog
### v8.0.2
* bug fix price, total value

### v8.0.1
* bug fixes
* add some hidden beta features for testing

### v8.0
* rewritten all API logic for new ID system to work properly
* added option to add sfmcContactKey to the mapping to use the contact in AddToJourneyBuilderOnJoin (segment integration with JB)

### v7.7
* added purchase option to simulate conversion

### v7.6
* bug fixes for CORS problems in a few websites when sending actions

### v7.5
* new text and html replacements for personalize objects

### v7.4
* Added categoryID for mapping/tracking
* Fixes for twreceiver and pr calls with ecodeURIComponent on user.id (to account for emails like julianopolito+1@gmail.com)

### v7.3
* Using Greasemonkey GM_xmlhttpRequest for time events API calls, because of CORS issues

### v7.2
* Added more error handling to ajax calls

### v7.1
* Bugfix: productID in mapping was not replacing regex groups

### v7
* Trigger product visits from the script by associating productID to a mapping
* Trigger product view time if staying in the page. For demo purposes, every 10 seconds represent 1 minute

### v6
* Ability to use groups in URL regexp patterns and replace in strings in mappings. {:1}, {:2}, {:3} - Check mapping example 1.5 (commented out)

### v5
* mapURL can now be set as string for exact match (same as before) or RegExp object for dynamic patterns
* Improved logging with domains for advanced cases where topFrame set to false (will personalize all frames in the page, and run multiple instances of the script - useful in specific use cases)
* Added style attribute to personalize objects, allowing for further customization of the html being injected
* Added advanced.addType attribute to the mapping object, allowing for multiple content injection types. Default is replace, but now it is possible to append or prepend the content
      * replace, append, prepend, after, before
      Append, and prepend will add the image inside the container for the selector
      After and before will add the content to the same html level as the selector object
* Improved mapping error and validation handling
* Now you can ommit the personalize object if you just want to capture action
* Added fake css class to elements injected for further debug _-_demojp_-_
* If no id is set, a random anonId will be generated per refresh